<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Advanced_Media
 * @subpackage Dachcom_Advanced_Media/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Advanced_Media
 * @subpackage Dachcom_Advanced_Media/admin
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Advanced_Media_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * This ist required to register Plugin in Dachcom Repo
     *
     * @var DachcomPluginObserver_Connector
     */
    private $dpo_connector;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    public function register_plugin() {

        $this->dpo_connector = new DachcomPluginObserver_Connector();
        $this->dpo_connector->connect_to_plugin_checker( $this->plugin_name, plugin_dir_path( dirname( __FILE__ ) ) . $this->plugin_name . '.php' );

    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dachcom-advanced-media-admin.css', array(), $this->version, 'all' );

    }

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dachcom-advanced-media-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * @var $ID attachment id
     * @return string
     */
    public function after_delete_attachment( $ID ) {

        $post = get_post($ID);

        if( empty( $post ) )
            return $ID;

        if( !strstr($post->post_mime_type, 'pdf') )
            return $ID;

        $file_path = get_attached_file($ID);

        if( empty( $file_path ) )
            return $ID;

        $base_name = basename( $file_path );
        $pdf_path = str_replace( $base_name, '', $file_path );

        $thumb_name = str_replace('.pdf', '', $base_name);
        $thumb_name = $thumb_name . '-150x150.jpg';

        $path =  $pdf_path . '/' . $thumb_name;

        if(file_exists( $path ))
            wp_delete_file( $path );

        return $ID;

    }

    /**
     * Create an PDF Preview Thumb, if possible.
     * @param $attr
     * @param $attachment
     * @return mixed
     */
    function rewrite_for_pdf_preview($attr, $attachment, $size) {

        //since wp 3.5;
        $is_js = isset( $attr['mime'] ) ? TRUE : FALSE;

        if ( $is_js && strpos($attr['mime'], 'pdf') === FALSE)
            return $attr;
        else if( !$is_js && strpos($attachment->post_mime_type, 'pdf') === FALSE )
            return $attr;

        $wp_upload_dir = wp_upload_dir();

        $meta = wp_get_attachment_metadata($attachment->ID);
        $path_parts = pathinfo(get_attached_file($attachment->ID) );

        $namebase = basename($path_parts['basename'], ".{$path_parts['extension']}");

        //officialy thumbnail size...
        $suffix = "150x150";

        $url = dirname(wp_get_attachment_url($attachment->ID));

        $file_url = "{$url}/{$namebase}-{$suffix}.jpg";
        $file_path = "{$path_parts['dirname']}/{$namebase}-{$suffix}.jpg";

        //echo $file_path;
        if(file_exists($file_path)) {

            if( $is_js ) {

                $attr['url'] = $file_url;
                $attr['type'] = 'image';

                $attr['sizes'] = array(
                    'thumbnail'=> array(
                        'height' => 150,
                        'orientation'=> "landscape",
                        'url'=> $file_url,
                        'width'=> 150
                    )
                );

            } else {

                $attr['src'] = $file_url;
            }


        }

        return $attr;

    }

    public function wp_get_pdf_attachment_metadata( $data, $postID ) {

        if( $postID !== 108)
            return $data;

        $file_path = get_post_meta( $postID, '_wp_attached_file', true );

        if( empty( $file_path ) )
            return $data;

        $file_type = wp_check_filetype( $file_path);

        if( !empty( $file_type ) && $file_type['ext'] == 'pdf') {

            $file_path = get_attached_file( $postID );

            $base_name = basename( $file_path );
            $thumb_name = str_replace('.pdf', '', $base_name);
            $thumb_name = $thumb_name . '-150x150.jpg';

            return array('thumb' => $thumb_name);


        }

        return $data;

    }

    /** Function wp_generate_attachment_metadata calls wp_read_image_metadata which
	* gives us a hook to get the target filename.
	*/
	public function filter_read_image_metadata($metadata, $file ) {

        global $ime_image_file;

        $ime_image_file = $file;

        return $metadata;
    }

    public function filter_attachment_metadata($metadata, $attachment_id) {

        global $ime_image_file;

        if( empty($ime_image_file) ) {

            $ime_image_file =  get_attached_file($attachment_id);

        }

        $this->create_pdf_sizes($ime_image_file);

        return $metadata;

    }

    private function create_pdf_sizes($image_file) {

        //no imagick available. :/
        if( !class_exists( 'Imagick' ) )
            return FALSE;

        //we need to add this into the mam options...
        $sizes = array('thumbnail' => array( 'width'=> 150 , 'height' => 150) );

        $info = pathinfo($image_file);

        if($info['extension'] != 'pdf') {

            return false;

        }

        $dir = $info['dirname'];
        $ext = $info['extension'];
        $namebase = basename($image_file, ".{$ext}");

        $fn = "{$dir}/{$namebase}.{$ext}[0]";

        $src_img = new Imagick($fn);


        //cmyk to rgb baby...
        if ($src_img->getImageColorspace() == Imagick::COLORSPACE_CMYK) {

            $profiles = $src_img->getImageProfiles('*', false);
            $has_icc_profile = (array_search('icc', $profiles) !== false);

            if ($has_icc_profile === false) {

                $icc_cmyk = file_get_contents(MAM_BASE . '/core/profiles/USWebUncoated.icc');
                $src_img->profileImage('icc', $icc_cmyk);
                unset($icc_cmyk);

            }

            $icc_rgb = file_get_contents(MAM_BASE . '/core/profiles/AdobeRGB1998.icc');
            $src_img->profileimage('icc', $icc_rgb);
            unset($icc_rgb);

        }

        $im = new Imagick();
        $im->newImage($src_img->getImageWidth(),
            $src_img->getImageHeight(),
            new ImagickPixel("white")
        );

        $im->compositeImage($src_img, Imagick::COMPOSITE_OVER, 0, 0);

        $im->setCompression( 9 );
        $im->setCompressionQuality(100);

        $im->setImageBackgroundColor('white');

        foreach($sizes as $size_name => $size_vals) {

            $suffix = "{$size_vals['width']}x{$size_vals['height']}";
            $dest = "{$dir}/{$namebase}-{$suffix}.jpg";

            //$im->resizeImage($size_vals['width'],$size_vals['height'], imagick::FILTER_LANCZOS, 1);
            //do not touch the proportion. so height is 0;
            $im->resizeImage($size_vals['width'], 0, imagick::FILTER_LANCZOS, 1);

            $im->setImageFormat('jpg');

            $im->writeImage($dest);

            chmod($dest, 0644);

        }

        $im->clear();
        $im->destroy();

    }

    /**
     * Add your Settings here, if want to extend the Dachcom Plugins
     */
    public function plugin_core_settings_api_init( ) {

        /*
        * SECTIONS
        */

        add_settings_section(
            'dachcom-advanced-media-section',
            __('Dachcom Advanced Media Settings', 'dachcom-advanced-media'),
            array($this, 'setting_section_callback'),
            'dachcom-services'
        );

        /*
		* SETTINGS FIELDS
		*/

        add_settings_field(
            'dam_setting-image_sizes',
            'Download Watchdog',
            array($this, 'setting_image_sizes_callback'),
            'dachcom-services',
            'achcom-advanced-media-section'
        );

    }

    /**
     * Settings API for File Handler Options
     */


    /* SECTIONS */
    function setting_section_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dam-settings-section.php';
    }

    /* SETTINGS FIELDS */
    function setting_image_sizes_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dam-setting-image-sizes.php';
    }

}
