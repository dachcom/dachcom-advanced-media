<?php

/**
 * The plugin bootstrap file
 *
 * @link              http://www.dachcom.com
 * @since             1.0.0
 * @package           Dachcom_Advanced_Media
 *
 * @wordpress-plugin
 * Plugin Name:       Dachcom Advanced Media
 * Plugin URI:        http://www.dachcom.com
 * Description:       Extends the media library of Wordpress.
 * Version:           1.7.1
 * Author:            Stefan Hagspiel
 * Author URI:        http://www.dachcom.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dachcom-advanced-media
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dachcom-advanced-media-activator.php
 */
function activate_dachcom_advanced_media() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-advanced-media-activator.php';
	Dachcom_Advanced_Media_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dachcom-advanced-media-deactivator.php
 */
function deactivate_dachcom_advanced_media() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-advanced-media-deactivator.php';
	Dachcom_Advanced_Media_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dachcom_advanced_media' );
register_deactivation_hook( __FILE__, 'deactivate_dachcom_advanced_media' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-advanced-media.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dachcom_advanced_media() {

	$plugin = new Dachcom_Advanced_Media();
	$plugin->run();

}
run_dachcom_advanced_media();
