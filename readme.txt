=== Plugin Name ===
Plugin Name: Dachcom Advanced Media

== Description ==

* Creates an PDF Preview Image

== Installation ==

1. Upload to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.7.1 =
* No Imagick Class Fallback return false
= 1.7.0 =
* Integrated in Dachcom Environment
* Core Improvements
* Better File Structure
= 1.6.6 =
* Fixed Upload issue, ready for wp 4.0
= 1.6.5 =
* ACF 5.0 Improvements, don’t show image editor in modal windows.
= 1.6.4 =
* bugfixed
= 1.6.3 =
* improved layout
* bug fixing
* added api.php
= 1.6.2 =
* Bug fixes class delegations
= 1.6.1 =
* Bug fixed: because of rename "mam.php" to "mrs-mam.php" but not updated in the functions
= 1.6 =
* Bug Fixing for 3.9.1
= 1.0 =
* A change since the previous version.
* Another change.