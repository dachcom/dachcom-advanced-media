<?php

class DamHelper
{

    static $el_name_prefix;

    public static function getInstance()
    {

        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;

    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

    public static function dam_get_file_mime_type($filename) {

        if(!file_exists($filename))
            return '';

        $size = getimagesize($filename);
        return isset($size['mime']) ? $size['mime'] : '';

    }

    public static function dam_supports_imagetype($mime_type) {

        if(function_exists('imagetypes')) {

            switch($mime_type)
            {
                case 'image/jpeg': return (imagetypes() & IMG_JPG) != 0;
                case 'image/png': return (imagetypes() & IMG_PNG) != 0;
                case 'image/gif': return (imagetypes() & IMG_GIF) != 0;
                default: return FALSE;
            }

        } else {

            switch($mime_type)
            {
                case 'image/jpeg': return function_exists('imagecreatefromjpeg');
                case 'image/png': return function_exists('imagecreatefrompng');
                case 'image/gif': return function_exists('imagecreatefromgif');
                default: return FALSE;
            }

        }

    }

    public static function dam_check_if_is_shared_media( $attachmentID = null, $returnID = FALSE ) {

        if( empty( $attachmentID ) ) {

            throw new Exception('dam_check_if_is_shared_media requested with empty attachment id');
            return FALSE;

        }

        $is_shared = get_post_meta( $attachmentID, '_is_shared', true);

        //nope, it's a standalone image
        if( !$is_shared )
            return FALSE;

        if( $returnID )
            return (int) $is_shared;

        return TRUE;


    }


}