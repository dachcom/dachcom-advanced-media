<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Advanced_Media
 * @subpackage Dachcom_Advanced_Media/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dachcom_Advanced_Media
 * @subpackage Dachcom_Advanced_Media/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Advanced_Media_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
